# archdeploy

## Description

archdeploy is a bash script designed to help (me) install and configure systems via the Archlinux installation media. The intention is to make deploying Archlinux systems easier, but also retain a minimalist approach. Ideally, this script will help with reducing clutter on Arch systems, as a saved configuration of archdeploy can create a nix.conf-like level of reproducibility.

## Features

archdeploy enables btrfs by default, using the same default setup found in the archinstall script. Packages are installed as listed in a nix.conf-like $packages variable in the script, allowing for simple, configurable, and reproducible Arch systems.

## Usage

Before beginning, set up your disk partitions. archdeploy will need to be pointed to a boot partition, a root partition, and a swap partition.

Edit arch.conf and replace the Root_partition, EFI_partition, Swap_partition variables with the corresponding block device names you wish to point them to. Change the other paramaters to tune your install to your needs.

When ready, run:

    # ./archdeploy start

The arch.conf file will be copied to the /etc/ directory on your root partition.

## License

archdeploy is licensed under the GNU General Public License Version 3.
