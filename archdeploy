#!/bin/bash

Hostname=archlinux
Root_partition=vda2
EFI_partition=vda1
Swap_partition=vda3
Use_btrfs=yes
Create_user=yes
Is_superuser=yes
Compression=lzo
Use_default_subvolumes=yes
Base_packages='
networkmanager
base
linux
linux-firmware
btrfs-progs
grub
efibootmgr
'
Packages='
vim
man-db
'

disksetup() {
  source ./arch.conf
  mkswap -f /dev/$Swap_partition
  mkfs.vfat -F 32 /dev/$EFI_partition
  swapon /dev/$Swap_partition
  if [[ "$Use_btrfs" = "yes" ]]; then
    mkfs.btrfs -f /dev/$Root_partition
    if [[ "$Use_default_subvolumes" = "yes" ]]; then
      mount --mkdir /dev/$Root_partition /btrfs
      btrfs subvolume create /btrfs/@
      btrfs subvolume create /btrfs/@home
      btrfs subvolume create /btrfs/@pkg
      btrfs subvolume create /btrfs/@log
      btrfs property set /btrfs compression $Compression
      mount -o subvol=/@ --mkdir /dev/$Root_partition /mnt/
      mount -o subvol=/@home --mkdir /dev/$Root_partition /mnt/home
      mount -o subvol=/@pkg --mkdir /dev/$Root_partition /mnt/var/cache/pacman/pkg
      mount -o subvol=/@log --mkdir /dev/$Root_partition /mnt/var/log
      mount --mkdir /dev/$Root_partition /mnt/btrfs
      umount -R /btrfs
    elif [[ "$Use_default_subvolumes" = "no" ]]; then
      mount /dev/$Root_partition /mnt/ 
      btrfs property set /mnt compression $Compression
    fi 
  elif [[ "$Use_btrfs" = "no" ]]; then
    mkfs.ext4 -F /dev/$Root_partition 
    mount /dev/$Root_partition /mnt/
  fi &&
  mount --mkdir /dev/$EFI_partition /mnt/boot
  mkdir /mnt/etc/ 
  genfstab -U /mnt/ > /mnt/etc/fstab

  install
}

install() {
  source ./arch.conf
  base_pkgs="$(echo "$Base_packages" | grep -v '#')"
  reflector --latest 10 --sort rate --save /etc/pacman.d/mirrorlist
  cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
  pacman -Syy
  yes \n | pacstrap -K /mnt $(echo $base_pkgs)
  rsync -rauv ./root/* /mnt/
  if [[ "$Use_btrfs" = "no" ]]; then
    cat root/etc/mkinitcpio.conf | sed -r s/'MODULES=\(btrfs\)'/'MODULES=\(\)'/ | sed -r s/'BINARIES=\(\/usr\/bin\/btrfs\)'/'BINARIES=\(\)'/ > /mnt/etc/mkinitcpio.conf
    cat root/etc/default/grub | sed -r s/'rootfstype=btrfs'// > /mnt/etc/default/grub
  fi &&
  echo "$Hostname" > /mnt/etc/hostname

  cp ./archdeploy /mnt/archdeploy
  cp ./arch.conf /mnt/etc/arch.conf

  arch-chroot /mnt/ sh /archdeploy postinstall
}

postinstall() {
  source /etc/arch.conf
  pkgs="$(echo "$Packages" | grep -v '#')"
  ln -sf /usr/share/zoneinfo/America/Detroit /etc/localtime
  locale-gen
  hwclock --systohc
  pacman -Syy
  pacman-key --init
  pacman-key --populate archlinux
  mkinitcpio -P
  grub-install --efi-directory=/boot /dev/$bootdisk
  grub-mkconfig -o /boot/grub/grub.cfg
  pacman -Syu $(echo $pkgs) --noconfirm
  if [[ "$Create_user" = "yes" ]]; then
    echo -e '\n Enter username: \n'
    read Username
    if [[ "$Is_superuser" = "yes" ]]; then
      useradd -mG wheel $Username
    elif [[ "$Is_superuser" = "no" ]]; then
      useradd -m $Username
    fi
    passwd $Username
  fi
}

case "$1" in 
  start) disksetup ;;
  disksetup) disksetup ;;
  install) install ;;
  postinstall) postinstall ;;
esac
